
public class BankAccount{
	
	public BankAccount(double balance, double interestRate) {
	this.balance = balance;
	
	rate = interestRate;
	
	}

	public BankAccount(long accountNumber, double balance, double interestRate) {
		this.accountNumber = accountNumber;
		this.balance = balance;
		rate = interestRate;
		
	}

	private long getAccountNumber() {
	
		return accountNumber;
	}

	public double getBalance() {
		
		return balance;
	}

	public double getInterestRate() {
		return rate;
	}

	public boolean withdraw(double amount) {
	// check to see if the mount is not negative and see if you have enough money
	
	if (amount > 0 && amount < balance) {
		balance -=  amount;
		
		return true;
				
	} else {
		System.out.println("Not enough money ");		
		return false;
				}
	}
	
	

	public boolean deposit (double amount) {
		// check to see if the mount is not negative
		if (amount > 0) {
			balance +=  amount;
			return true;
					
		} else {
			System.out.println("You enter a negative amount " );
					return false;
					}
		}
	
	public double futureValue(int years) {

		double a = 1 + getInterestRate();
		double b = years;
		
		future = (getBalance() * Math.pow(a, b)) * 100;
		
		future = (int) future;
		future /= 100.00;
	return future;	
	}
	
	
	

	private double rate, balance, future;
	private long accountNumber;
}

