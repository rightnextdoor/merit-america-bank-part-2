public class CDOffering{

	public CDOffering (int term, double interestRate) {			
		this.term = term;
		this.interestRate = interestRate;
		
	}

	public int getTerm() {
		return term;
	
	}
	
	public double getInterestRate() {
		return interestRate;
	
	}
	private int term;
	private double interestRate;
}
