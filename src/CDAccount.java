
import java.util.Date;

public class CDAccount extends BankAccount {
	
	public CDAccount(CDOffering offering, double balance) {
		super(getAccountNumber(), balance, offering.getInterestRate());
		term = offering.getTerm();
		rate = offering.getInterestRate();
		this.balance = balance;
	}
	public CDAccount () {
		super(getAccountNumber(), getBalance(), getInterestRate());
	}
	

	public double getBalance() {
		return balance;

	}

	public double getInterestRate() {
		return rate;

	}

	public int getTerm() {
		return term;

	}

	public long getStartDate() {
		Date date = new Date();
		
		return date.getTime();
		
		}

	public long getAccountNumber() {
		
		return getStartDate();
		

	}

	public double futureValue() {
		double future = futureValue(term);
		return future;
		}
	
	public MeritBank bank = new MeritBank();
	private long account = 0;
	private static double balance, rate;
	private int term;
}

