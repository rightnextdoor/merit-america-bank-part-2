
public class MeritBank {
	
	public static void addAccountHolder(AccountHolder accountHolder) {
		accounts[accounts.length - count] = new AccountHolder();
		count++;
	}
	
	public static AccountHolder[] getAccountHolders() {
		return accounts ;
	}

//-----------------------------------------------------------------------
// CD

	// I'm guessing this is where you get the best to offering and put it in an array
	public static CDOffering[] getCDOfferings() {
		
		/* 1 year term at 1.8%
		 * 2 year term at 1.9%
		 * 3 year term at 2.0%
		 * 5 year term at 2.5%
		 * 10 year term at 2.2%
		 */
	
		double first, second, next;
		next = 0;
		double rate[] = {rate3, rate4, rate5};
		
		// comparing the rate 1 and rate 2 to see the highest value
		if (rate1 > rate2) {
			first = rate1;
			second = rate2;
		} else {
			first = rate2;
			second = rate1;
		}
		// loop the last 3 rate number
		for (int i = 0; i < 3; i++) {
			// check the array number to the first
			if (rate[i] > first) {
				first = next; // if the array is bigger then put first number to next
				first = rate[i]; // array become the new first
				if (next > second) { // check if next number is bigger then the second number
					next = second;  // replace second if bigger
				}
				
			} else if (rate[i] > second) { // array was lest then first then check the second number
				second = rate[i];
				
			}
			
		}
		       CD = {first, second}; // put the two highest value in an array
		
		// find the term and interest rate for the to array value
				int term;
				double interestRate;
		       for (int i = 0; i < CD.length; i++) {
				
					if (CD[i] == rate1) {
						term = 1;
						interestRate = rate1;
						return (CD[i] = {term, interestRate});
						
					} else if (CD[i] == rate2) {
						term = 2;
						interestRate = rate2;
						return (CD[i] = {term, interestRate});
						
					} else if (CD[i] == rate3) {
						term = 3;
						interestRate = rate3;
						return (CD[i] = {term, interestRate});
						
					} else  if (CD[i] == rate4) {
						term = 5;
						interestRate = rate4;
						return (CD[i] = {term, interestRate});
						
					} else {
						term = 10;
						interestRate = rate5;
						return CD = {term, interestRate};
						}
					}
		
	} 
	
	
	public static CDOffering getBestCDOffering(double depositAmount) {
		
		return first;
		}
	
	public static CDOffering getSecondBestCDOffering(double depositAmount) {
		
		return second;
		
	}
	
	public static void clearCDOfferings() {
		CD[1-clearCount] = null;
		clearCount++;
	}
	
	public static void setCDOfferings(CDOffering[] offerings) {
		// trying to put term and interest rate into offerings
		CDAccount cdAccount = new CDAccount();
		for(int i = 0; a < CD.length; i++) {
			offerings = CD[i];
			clearCDOfferings();
			// get the future value from CDAccount class to put into best and second best offering
			if (i == 0) {
				first = cdAccount.futureValue();
			} else {
				second = cdAccount.futureValue();
			}
			
		}
	}
	
//-------------------------------------------------------------------------
// Account information	
	
	public static long getNextAccountNumber() {
		
		}
	
	public static double totalBalances() {
			total = accounts.getCDBalance() + accounts.getCombinedBalance();
			return total;
	}
	
	public static double futureValue(double presentValue, double interestRate, int term) {
		
	}
	
	/* 1 year term at 1.8%
	 * 2 year term at 1.9%
	 * 3 year term at 2.0%
	 * 5 year term at 2.5%
	 * 10 year term at 2.2%
	 */

	static CDOffering[] CD = new CDOffering[2];
	static AccountHolder[] accounts = new AccountHolder[10];
	private static double first, second, total;
	private static double rate1 = .018;
	private static double rate2 = .019;
	private static double rate3 = .020;
	private static double rate4 = .025;
	private static double rate5 = .022;
	
	
	private static int count = 0;
	private static int clearCount = 0;
	
}
