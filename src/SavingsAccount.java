public class SavingsAccount extends BankAccount {

	public  SavingsAccount(double balance) {
		super(balance, 0.01);
		//futureValue(3);
		
	System.out.println(toString());
	}

	public String toString() {
		return ("Saving Account Balance: " + getBalance()  + "\n" + "Saving Account Interest Rate: "
				+ getInterestRate() + "\n" + "Saving Account Balance in 3 years: " + futureValue(3) + "\n");
	}
	
}