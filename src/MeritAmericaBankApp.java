public class MeritAmericaBankApp {
	
	
	public static void main(String[] args) {
		
		AccountHolder account = new AccountHolder();
		//CheckingAccount checking = new CheckingAccount();
		
		account.AccountHolder("John ", "James ", "Doe ", "123-45-6789 "); 
		account.addSavingsAccount(500);
		account.addCheckingAccount(600);
		account.addSavingsAccount(5000);
		account.addCheckingAccount(6000);
		account.addSavingsAccount(300);
		account.addCheckingAccount(600000);
		
		AccountHolder ah2 = new AccountHolder();
		ah2.AccountHolder("Bob ", "Ben ", "David ", "455-45-575");
		ah2.addSavingsAccount(400);
		ah2.addCheckingAccount(900);
		
		
		System.out.println(account.toString());
		System.out.println("-------------------\n");
		System.out.println(ah2.toString());
	}
}