public class CheckingAccount extends BankAccount {

	public CheckingAccount(double balance) {
		super(balance, 0.001);
		//futureValue(3);
		System.out.println(toString());
		
	}
	
	public String toString() {
		
		return ("Checking Account Balance: " + getBalance()  + "\n" + "Checking Account Interest Rate: "
				+ getInterestRate() + "\n" + "Checking Account Balance in 3 years: " + futureValue(3) + "\n");
	}

}