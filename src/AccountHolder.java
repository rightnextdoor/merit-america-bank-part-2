
public class AccountHolder{
	
	private static final int MAX_BALANCE =	250000;

	public void AccountHolder(String firstName, String middleName, String lastName, String ssn) {
		
		this.firstName = firstName;
		this.middleName = middleName;
		this.lastName = lastName;
		this.ssn = ssn;
		
		
	}

	public String getFirstName() { 
		return firstName;
	}
	
	public void setFirstName() {
		getFirstName();
	}

	public String getMiddleName() {
		return middleName;
	}
	
	public void setMiddleName() {
		getMiddleName();
	}

	public String getLastName() {
		return lastName;
	}

	public void setLastName() {
	getLastName();
	}
	
	public String getSSN() {
		return ssn;
	}
	
	public void setSSN() {
		getSSN();
	}

	//----------------------------------------------------------------------------------------------
	// checking account
	
	 public CheckingAccount addCheckingAccount(double openingBalance) {
		
		 if(openingBalance > 0) {  
			if((openingBalance + getCombinedBalance()) < MAX_BALANCE) {
			 countChecking++;
			 checkBalance = openingBalance + getCheckingBalance();
			 numberOfCheckingAccounts[numberOfCheckingAccounts.length - countChecking] = new CheckingAccount(openingBalance);
			 System.out.println("Checking Balance " + getCheckingBalance() + "\n");
			 return new CheckingAccount(openingBalance);
			} else {
				System.out.println("Your total balance is over $250,000");
			}
			return null;
			}  else {
				System.out.println("You enter a negative number");
				return null;
				}
		 }
	 
	 
	 
	 
	 public CheckingAccount addCheckingAccount(CheckingAccount  checkingAccount) {
		return checkingAccount;
	
	 }
	
	 public CheckingAccount[] getCheckingAccounts() {
		return numberOfCheckingAccounts;
	   }
	
	 public int getNumberOfCheckingAccount() {
		return countChecking;
			}
		
	 public double getCheckingBalance(){
		 return checkBalance;
			}
		
//----------------------------------------------------------------------------------------------
// saving account

	public SavingsAccount addSavingsAccount(double openingBalance){
		
		if(openingBalance > 0) {
			if((openingBalance + getCombinedBalance()) < MAX_BALANCE) {
			 countSaving++;
			 savingBalance = openingBalance + getSavingsBalance();
			 numberOfSavingAccounts[numberOfSavingAccounts.length - countSaving] = new SavingsAccount(openingBalance);
			 System.out.println("Saving Balance " + getSavingsBalance() +"\n");
			 return new SavingsAccount(openingBalance);
			} else {
				System.out.println("Your total balance is over $250,000");
			}
			return null;
		} else {
			System.out.println("You enter a negative number");
			return null;
		}
	}
	
	public SavingsAccount addSavingsAccount(SavingsAccount savingsAccount) {
		return  savingsAccount;
	}
	
	public SavingsAccount[] getSavingsAccount() {
		return numberOfSavingAccounts;
	}
	
	public int getNumberOfSavingsAccounts() {
		return countSaving;
	}
	
	public double getSavingsBalance() {
		return savingBalance;
	}
	
	
//----------------------------------------------------------------------------------------------
// CD account	
	
	
	public CDAccount addCDAccount(CDOffering offering, double openingBalance) {
		
		if(openingBalance > 0) {
			CDbalance += openingBalance;
			countCD++;
			numberOfCDAccount[numberOfCDAccount.length - countCD] = new CDAccount(offering,openingBalance);
			return new CDAccount(offering, openingBalance);
		} else {
			System.out.println("You enter a negative number");
			return null;
		}
	}
	
	public CDAccount addCDAccount(CDAccount cdAccount) {
		return cdAccount;
	}
	
	public  CDAccount[] getCDAccounts() {
		return numberOfCDAccount ;
	}
	
	public int getNumberOfCDAccounts() {
	return countCD;
		}
	
	public double getCDBalance() {
		return CDbalance;
		
	}
	

//----------------------------------------------------------------------------------------------
// Combined balance
	
	public double getCombinedBalance() {
		double total = 0;
		total = getCheckingBalance() + getSavingsBalance();
			
		return total;
		
	}
 
		
	
//----------------------------------------------------------------------------------------------
// toString

	public String toString() {
		
		
		return ("Name: " + getFirstName() + getMiddleName() + getLastName() + "\n" + "SSN: " + getSSN()
				+ "\n" + "Total Balance " + getCombinedBalance());// "\n"
				//+ checking.toString() + "\n" + saving.toString());
	}

	private String firstName, middleName, lastName, ssn;
	private double checkBalance, savingBalance, CDbalance;
	private int countChecking = 0;
	private int countSaving = 0;
	private int countCD = 0;
	CheckingAccount[] numberOfCheckingAccounts = new CheckingAccount[10];
	SavingsAccount[] numberOfSavingAccounts = new SavingsAccount[10];
	CDAccount[] numberOfCDAccount = new CDAccount[10];
}